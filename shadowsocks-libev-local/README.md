
## Example

Example inventory file:

``` ini
# ss-hosts.yaml
ss-locals:
  hosts:
    localhost:
      ansible_connection: local
      ansible_become: true
  vars:
    ss_connections:
      server1:
        server: server1.example.com
        server_port: 23333
        local_port: 1080
        method: chacha20-ietf-poly1305
        password: password1
      server2:
        server: server2.example.com
        server_port: 23333
        local_port: 1081
        method: chacha20-ietf-poly1305
        password: password2
```

Example playbook file:

``` yaml
# ss-locals.yml
- hosts: ss-locals
  roles:
  - shadowsocks-libev-local
```

To run the playbook:

``` bash
ansible-playbook -i ss-hosts.yaml ss-locals.yml
```
