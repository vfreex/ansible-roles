
## Example

Example inventory file:

``` ini
# ss-hosts.yaml
ss-servers:
  hosts:
    localhost:
      ansible_connection: local
      ansible_become: true
  vars:
    ss_connections:
      instance1:
        server: 0.0.0.0
        server_port: 23333
        method: chacha20-ietf-poly1305
        password: password1
      instance2:
        server: ::
        server_port: 23334
        method: chacha20-ietf-poly1305
        password: password2
```

Example playbook file:

``` yaml
# ss-servers.yml
- hosts: ss-servers
  roles:
  - shadowsocks-libev-server
```

To run the playbook:

``` bash
ansible-playbook -i ss-hosts.yaml ss-servers.yml
```
